$(document).ready(function(){
    $('.link-point').on('click', function () {
        if (!$(this).hasClass('active')){
            $(this).addClass('active').prevAll('a').addClass('active');
            $(this).nextAll('a').removeClass('active');
        } else{
            $(this).prevAll('a').addClass('active');
            $(this).nextAll('a').removeClass('active');
        }
        $($(this).data('return')).attr('value',$(this).data('value'));
    });

    $('[data-toggle="popover"]').popover()

    $('.scrollbar-inner').scrollbar();
});